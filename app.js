new Vue({
    el: "#game",
    data: {        
        pontosGanhos: 0,
        isEditingName: false,
        gameover: true,
        fightingMonster: [],
        monsterStatus: 0,
        playerStatus: 0,
        winner: '',  
        showButtons: false,
        actions: ['Standing', 'Atacking', 'Defending'],
        playerAction: '',
        monsterAction: '', 
        bloquearPontos: false,
        playerAgil: '',
        monsterAgil: '',
        game: {
            victory: false,
            message: "",
            defeat: 0
        },
        items: {
            Life: {
                img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnq-3KoIHuD0fF495Ieolo4vp6AQkJTthbYmhTC5NWB_Zvu2eS",
                nome: 'Life',
                power: 40,                
                price: 50
            },
            Bomb: {
                img: "https://d1nhio0ox7pgb.cloudfront.net/_img/o_collection_png/green_dark_grey/256x256/plain/bomb.png",
                nome: 'Bomb',
                power: 30,
                price: 135
            },
            Sword: {
                img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBK9oitIROjQYoTNicfTLlBTL0DqM8HV-_UxdG4vB97T43Qubefw",
                nome: 'Sword',
                power: 50,
                price: 180
            }
        },
        monsters: [
            {                
                img: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIPEA8PEBAQEA8PEBAQEA8QDxAPFQ8VFRUYFxUXFhUYHSggGBolGxYWITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQFysdHx4rLSstLS0tLS0tLSstLS0tLS0rLS0rLS0tLS0rLS0tLS0tKy0tLS0tLS0tLTcrKy03K//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQIFAwQGBwj/xABEEAABAwIDBQQHBQUFCQAAAAABAAIRAwQSITEFBkFRYRMicYEHFDJCUpGhI7HB0fBicoKS4RUzQ2PxFlOTorKzwsPS/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAgEQEBAQEAAwADAAMAAAAAAAAAARECAxIhMUFRE0Jh/9oADAMBAAIRAxEAPwDrEIQvc8oQhMBQEJoTAUAnCITQJNCagSaEQiBCcIhFJCcIhEJJNCBITQqqJCRUkIIJQpQkgSSkUlQkIQqBCEIBNJMBQMBCFIKAATQmgSaE4UQoTATQgEIQi4EIQgEIQgEQhCGElCkhERQnCSBJQpJKqikVIhJBFJSKiqBCEKhhMITCyGE0JoBARCkogQhCKEJpgKBIhSVHvFvC21BYwB9cicPusB0Lvy+5BcVXtYC5zg1o1c4hoHmVVVd5rRpg1gT+yx7h8wIXn95Xr3LsVWoXu66M6NAyHksRt41Kmrj0X/aa0/3p/wCFV/8AlbdptWhWIFOtTc46NxQ7+U5rzBtLqfmihTDienAqmPWyELzix25cW7hheajONOoS4eR1b5Ltdkbdo3MNBwVYzovIDv4fiHh9ERYoUiEkCSTQqIlCkolEJIqSSqolRKmQooIoUoSV0STQE1ABNCYCiBNCEUJhAUgoABc7vndPpMtyxxb9tikEjNrSWgxqNcuK6MBef70bcFzVNKlBpUC7v643xBI/ZGYHPM8lCHtzfJ9WKdpNNh9qqR3jzDfhA56+CogJOZLiTLnEzJ5knVKhahoGpJknXU5lZqNGXYie4OHA9T0RU5Aa52jGgyefNY3jugZTAkeS0tqbQ7zaQybILjzzkDzhZWVOfzSfTCcfIo2e6XkLFc6a58PyUbJwDg4GZ1HJaqrd4DcRWk+mT3jII0PL8fNbL62InkNPwWxbQ9mY4kfJac9WWwd56lMincONWnpjOdRnWffH18dF3FOoHNDmkOa4SHAyCOi8rrszy1bmArnd7bnq7oefsHnv/wCUfjHTn8+GcsNd2QkpqJWWiSTQqIoTKSISjCkkVVRQmhAwmhNABSSCaiBNJSCigBSAQE0RVb0X3q9pXqAw7DgaeTn90HymfJeX7MpyzFoHZt6NGQ+4rqvSptENpU7dp75JqubyABa2fEuP8q4TZ9d0NAL8BzZLRJ6QDmAs7lak+a6JlGYicyAI4rR2tc4GvaCOAGfAa+crRr39RmQJDS7MgSWjiQPM5LUFQPe0NxFoEOdUhpc6TJDQchEAcdZ5CXv9RZxc2pMs3vl5jpJWy2qQIOoVh2jS0CWiBoFW3WXe/I/NST1JdZDUDhBWO2lr8/A+PA+a1n1M1tUnYgTx4+WYK1ui02a2THMq1tKXtjr+Ga0NkM9k8nffIVzbCHP8j9F15+ud+K6syD4mPktSl7Tm/qFY1s3H5LQY37R3gqR2W522mVKTLd7x29KWBrjBqMae6ROpAyPHKV0i8YvKBHeEy3vSJkZ6jlC9G3N2/wCuUi1/9/RgP/zAfZePHj18QuTdi/KSkQoqoFFSSKqEkmkqpITQgAmkEwoJIQhCGpKIUlAwqbenb7bGliydWeCKVM8Txc79kZeOQVySAJOQAknkF4xt/ajryvUqnRxhg+Cm2cI+8+JKlJNquvbl1VzqlZ+J9V0ve7qR8gMhHDJXTab2tbU7RoGGGu+zOQ8lSC0dVeKbSAcJOeYPQrQdRc0FuFj4cQaYdBEdJ0XP3kuOvpbPi8oNNUuxPkDVzs8+Q/XFRexlPIuac5jIk/itG0q1sOeCkAWtDS4Z4nRk1Xe8e73qdG2uXvL+1xNceGKJAA4ZByn+ST41PH1jTpVMpa3N3HjlzWpf18uEn6LQqbS4CfAZStY1i8ycunJS9MY2WulbtjUEEcwVo025fVSY+CPFSXGcdVsar3T0KuhUkzzauQ2fcYXROuS6GyrS0DiMl6eKz1EnOl0dSsLYxuPIBFaphcfKFoMuJc4fE4j5LduESvXYnQFn2FXNpcU6wyju1GfHTce9HUaxzAWvVaRDvmtluF4jQgZH9eaxJtrT1WjVbUaHscHNcJa4GQQmVwm5+1jSrervd9nUMAH3Hn2SOjtPGF3hUZRQhCpUSkVIpIhIQhVTQEICgkhCEEgpBRCkFEql3zvuwsLp8wTT7JvOahDBHhinyXkti2RPOV13pA2uK9UWrM6duS+qfiqkFob/AAgmepPJcrs86KN8z43tliLijPvS35tMfWFWbZthSrvOAOIdpJB8iPNZr6qWuDm5OaQ5vi0ghZt56gq9lcN0eIPQ6/muXfzuV25u8X/inqXDDOGmA7hqTPmV6dvEwXW7oqgAuouoVfDvBrvo53yXlrc8yu53a2r2mytpWhOlCsWjxaSPrK5eT9Vrx/ZZ/XnZpSpUacGCsoyGSg6lHe4yukjm3adGeMpXNDCFG0YDkcnDMHmtx7zEOE9Vc+M41aNTR3z8la2N5B8SFT1KZaZHySZW/wBOKvPWUsdNd1Zz8VjsWA/MlVTLskQfJb2zq+YB6rtstYW4pyM+JWhW7jxnlE5eOqtahEAcs/HJVlzBcJEhwjkdVq/gad3c94OaYc0yCOEaL1vYu0RdW9KuIl7e8B7rxk4fOV5XtK1YxjcMcYPMqx3E22bav6vUP2NcjX/DqaB3gcgfI8Fztyj09CEFaQikmUkQk0k0UgmoqQREkJIRUgtXbF56vb164gmlSe9oPFwHdHzhbQKo9+if7PuY5Uv+6xSjzV1E4S4yXYXOc7i5xMk+OpVdbXGB0HKSY655K8sHh7B9fxVJtezgEjVp+5S/2NldVg469Fs7Ntqt0DaUqZqPJa9vw084JcfdbnqqO0DsdNzvYL2TMiWlwn6cV9C7N2XRtWmnQptptmTEkuOkucc3HxKxnsvt6uGs/RcCB6xdvJjNtCm1oHTE+Z8YC2BuB6sH9hcPe2s3sHtqUwS0VO6Hy0iQ3FJEDKcxC71NLxGOfJ1LryHbu4dxaUX3Ha0atKiwvqRjpvAHJpBB/mXGPuMXgvoDeW3NWyvKQ1fbV2jxNN0fVfPzGd3yU6dOLs+slGq0e8s39oRliDh4rT7LJYXU1n2saxYm/YfyWtWumcj5LWbTWOtqperVxcbvWrryv2FNxY7sqtQYhixGmwuDcjxiJ4JbPvsRa4a8lv8AouB/tW2jg2vPUdk7+i1t6tlGyv7iiBDC81aXAdm84mgeGbf4VZc+s2fcX9O/BZJOgWk+5x1GNbmZE8eX4SqJrn6NOvA8V0uxNn4GhzsyRr0/qu06vXxizGbs5YXHVs4eOnFVFenMu4CPPn96ub+5Hst5EeCrKp7v7xwjwGp/XNXyfgen7n7VN1asc4zUpfZVCfeLQId5tIPjKu1wHoue7FeMPsjsXDxOP8APku+U5uxmzCKSZUStMmhJCKSkCoSnKCaEgUShEwVV72U8djdgaii9w8WDEP8ApVlKwbSI7CvOnY1Z8MBlZo8ds6pYZGmvzVlesbUaSOI/0VXYDEwDiB+Cmy7iWniJBSX43qnuR9iWgZgOA8eC+iB114+K8Es6eO5pMPvV6LY/eeAvei5Y5Z6SlMFQlErbDIvBd5Nivsbh9BzSGSXUH8KlOe6QeYEA8j5L3eVo7Z2RRvKfZXDMbQcTTJa5h5tcMws2a3z1leCASFiqsXqjvRnQxS25rNYfdLabnfzZD6K72LunaWR7RrC+o3PtqxD3NjUtyAb4gSs+rpe48KumupuLHMcx4iWvaWESARIOYyIPmtUhWG2Ls3NzcXB/xqr6g/dJ7v8AywtArnXSOy9EtLFtIH4Les754W/+S7D0q7ANxQZd0mzUtQ7tANXUjmSOeE5+BcqD0NUZubqp8Fu1g/jfP/rXqd3cspU31KhDabGlzydAAM8uPgunM+OPVzp4bsK3DvtHey3TqrateF3dbl4LTpva97hSYKdIuc4MGlNpcSG+QgLacQwdfuXSfIdVr1m5hg5948+ax1TqQCdGMAEk+A4rI2YLuJ7rV0+4exe0f63UEspEtoA+88e0/wAswOvgsfakro90tjG0oQ/++qkVKv7JjJo8B9ZV3KEiV0kxmgpJSkqGhJCCMpyoJoJynKxynKgnKrd5q+Cyu3creqB4lpA+pW/K5n0gXEWnZDWvVYz+Fpxu8u6B/Eosef2mQEalTq0cTSW+6fpxUgyFmb3Wkc8lbP06Yqm1eyrUK0wKdWk53DJrwZ+i95Ll4FtFmJrm8wV7PsO/7e2t6vF9Jhd+9EO+srE+MdRbYkYlr9ojtFphsYk8S1u1S7VDG1iXG+kreFtvbOtmOBr3LS0gHOnSOTnHlIlo8SeC1t4/SBSo4qdrhr1hIL5+ypnxHtnoMuvBeYXFy+vVdUqPL3vOJ73aud+so0ACx106c8seGGrXc1bFw7vAcBCx1hmVydY670WbZFvdmi4d27DKeLix7MRZ5HER5hX3pE252z/UaR7lNwdcOGheNGeDdT1j4V51s6k/tGOYYLHNcHDVrgZaR5j7lfCnAjUky5xzLicyZXTj8M9cy3WSzaGCAM+aVY56ySsjAQVh1qE8gu3XPyMdRncwuwU2CXOLadMc3OML1bZ1o23o06LfZpsDZ5kanzMnzXjle+fTqU6lP2qT2vYJiS0gkE9YjzXsFnesrU2VabsTHtDmn8D1GkLE/KWNuUiVjxpYlplkJSJUMSUqicprHKFQ5RKjCiVBklLEsRcoGoouM1SqGtc5xAa0FzieAGZK8u23th17XxGW0mA9mz4Wzx/aJif6K73t24+oH21BrntEdvUY1z4GuHu6dSeULiqNYtl/uCeOsafrqVj2mt88/wBbzHd90+4Bl1OihdVyHsaDrin5ErVrXBa0lzXNdUIeC4EZR3SOkfep29M3NWhTp51HGBJgdSeQVvcxvGKvU8yvRNwK59RYD7tSsG+GMn8SvOtr2VW2rOoVRDmkZtMtcDoQV3u6dTBaUxzdUd83lSXWe5jqDWUTcKtdclQNZ3Iq654sTcrmd7NpPeBbU3Ri9s8+MHpGZ5qwqF50aqGtsesXF+Zc6ZJ0E8h/VY72z46+P13a5DaVuyn3WZvnXi4/rgst1sg0KeJ7vtTmWiIb06q3dutUFVlU4nBr2vwkDMgzqtfa2yL2u90UHOBmGsLSY6yQuWWO3tzVLsi09YrYRkA1zj5LE6n3y34Zk9FZ2FvUs3Pxtw1H08IY5zJBJykAmBkfksLKYAxvybM4eL3cz+S1zLrPVmNyyYGNk5DXPWOa2aRyLjoBP5LWbm5jXRhAxkc/hb+KKt0HnC3Rzpy5Cfx+5d/kjm221p+cfRa4qAdoev0WKo7Ixrjao3mQAaCXHIAAkmByCl7rOI2NsK9RzXuIboCDGfErv90LH1Vr2YnEHCQMUt8cPA+C4K+sLh5pPo0qjIpskBuDvZzMxmuz3e9Za1nbBhPEtdmBpnwJ0K4z29tdrOfWx1orKQqLTY9ZmuXoeZsB6crEHKYKsEkJSmiMyUKUJwqMLmqv2hdtpAyCTgcQ1okuPAAceKt8KwVrCm8yW5xEgkGFjvnZjfHU5uuG3h2qyzoU2T9pVBc7PNxOpP3eS4qnfCpWphwAbUrUsQjIy8Ysuq9dut1bWrPa0+0LgGlxMOgad4QVrjcexgfYCRoS57iPrquM8Njt155b+HLelC4p9oymILizFHFoOQ/XRcRsraRoVmVBngiQNYBzhevV9yLarWdXqmrUe4AEF5DYAgQBmMuqzs3LsRpbUxHHDJ+ZTnw2TE6803Y4TeR/rtww0oc00qUO4DFLpJ6AhdTsmzwU2UxJDREkRPMq9tt3qFL2KYat9lsBoAunPGTGO+/a6rKVn0Wyy0HJbvZp4FrHNqigOSfYjktnAkaaYjTqMaASdAJPkqi42yaVN2BjnT7ZYxzwzpICvK9rjBaYIORB0K132T4hpa0DQAZDyXPvj2+O3i8npvz64Onbja1ZlFrojvVKsT2dLjHUkCJ68lsb0bv2tPCKUtNIaOfIdzmfe6q12Xu7UsnVzbjvVoOIyQyJyHTNc5tDdLaNSq6Q2ox+faGoxpg6gjh5TkuP+PuX5+I7Xvizb+a5azpOrXDKDTBc/DPzk/JXG+GyG7PfTFJ7iC0TigkGOY4HM6Kx2Rubd2t1SuHtpuptLsQpve5wkEfCJUNtbsbQ2hWfVNNtJkxTbVqAQ0ZDJs58fNbs79mZePVX09lubY07suBNV5hsZgcDM56adVYbr2Rc8XD2yGgimCNSci7wjJWlpuvdOo2ttVDW07cuLy10ipn3AJExBzXU2mxwwCTpyWuOb+2PJ1z/AKtKmCfd+i2qdBx4KyZbgaBZAF1xx1ostCszbVbCRCuGsfYhSwBShIhaChCIQqMiYSTREk1EJqCSaiE0DTBSQoiSFGU5QNCEIuhCEIBCEIBCEIaUIRKSIEimkgSSaSqkghCCgikQmhUKEJwhUCEIQNNRTUEkwoSmoJoUUwUEkJIUDRKSaByiUkIhyiUkICUISRTQkhUCEpSlA0kkEoBJCSoEIQqBCEKYBCEKgCEIQCkEkKUSQEIUDCaEIBCEIBBTQoEhCECTQhUCSEIEkhCAKSSFYBCEKgQhCAQhCo//2Q==",
                nome: "Leopard Knight",
                forca: 4,
                agilidade: 6,
                resistencia: 2,
                life: 100,
                pontos: 0,
                money: 0
            },
            {
                img: "https://previews.123rf.com/images/e71lena/e71lena1610/e71lena161000051/65208798-ogre-monster-3d-illustration-isolated-on-white.jpg",
                nome: "Ogro Grosso",
                forca: 7,
                agilidade: 2,
                resistencia: 5,
                life: 100,
                pontos: 0,
                money: 0
            },
            {
                img: "https://i1.sndcdn.com/artworks-000068908517-8klieq-t500x500.jpg",
                nome: "El Matador",
                forca: 2,
                agilidade: 8,
                resistencia: 2,
                life: 100,
                pontos: 0,
                money: 0
            },
            {
                img: "https://i.pinimg.com/736x/b6/56/de/b656de1d23c88dded976759dc31f67ff--dark-fantasy-fantasy-art.jpg",
                nome: "Magnata",
                forca: 8,
                agilidade: 2,
                resistencia: 2,
                life: 100,
                pontos: 0,
                money: 0
            }
        ],

        player: {
            nome: "Player 1",
            pontos: 12,
            maxpontos: 12,
            money: 25,
            skills: {
                forca: 0,
                agilidade: 0,
                resistencia: 0
            },
            items: [],
            life: 100
        }
    },

    created() {
        this.randomMonsterPoints()
    },

    watch: {
        'player.skills.forca'(val) {        
            
            let psk = this.player.skills;
            let sumskills = Number(psk.forca) + Number(psk.agilidade) + Number(psk.resistencia);            

            //checa se tem pontos a distribuir
            if(sumskills >= this.player.maxpontos) {
                sumskills = this.player.maxpontos;
                psk.forca = sumskills - (Number(psk.agilidade) + Number(psk.resistencia));
            }

            // Impede valores negativos
            if (Number(val) <= 0)
                psk.forca = 0;
            
            // Impede valores acima do máximo
            if (Number(val) > this.player.maxpontos)
                psk.forca = this.player.maxpontos;

            
            // if (Number(val) <= this.player.pontos)
            //     this.player.pontos = sumskills - Number(val);
            
            this.player.pontos = this.player.maxpontos - sumskills;
            
        },

        'player.skills.agilidade'(val) {

            let psk = this.player.skills;
            let sumskills = Number(psk.forca) + Number(psk.agilidade) + Number(psk.resistencia);

            //checa se tem pontos a distribuir
            if (sumskills >= this.player.maxpontos) {
                sumskills = this.player.maxpontos;
                psk.agilidade = sumskills - (Number(psk.forca) + Number(psk.resistencia));
            }

            // Impede valores negativos
            if (Number(val) <= 0)
                psk.agilidade = 0;

            // Impede valores acima do máximo
            if (Number(val) > this.player.maxpontos)
                psk.agilidade = this.player.maxpontos;

            this.player.pontos = this.player.maxpontos - sumskills;

        },

        'player.skills.resistencia'(val) {

            let psk = this.player.skills;
            let sumskills = Number(psk.forca) + Number(psk.agilidade) + Number(psk.resistencia);

            //checa se tem pontos a distribuir
            if (sumskills >= this.player.maxpontos) {
                sumskills = this.player.maxpontos;
                psk.resistencia = sumskills - (Number(psk.forca) + Number(psk.agilidade));
            }

            // Impede valores negativos
            if (Number(val) <= 0)
                psk.resistencia = 0;

            // Impede valores acima do máximo
            if (Number(val) > this.player.maxpontos)
                psk.resistencia = this.player.maxpontos;

            this.player.pontos = this.player.maxpontos - sumskills;

        },

        'player.pontos'(val) {
            let psk = this.player.skills;
            if (Number(val) <= 0 && psk.forca > 0 && psk.agilidade > 0 && psk.resistencia > 0) this.bloquearPontos = true;
            else this.bloquearPontos = false;
        }
    },

    methods: {

        beginGame(monster) {                  
            if (this.player.skills.forca > 0 && this.player.skills.agilidade > 0 && this.player.skills.resistencia > 0 && this.monsters[monster].life > 0 && this.player.life > 0) {
                this.gameover = false;            
                this.fightingMonster = this.monsters[monster];

                let timeout = setTimeout(() => {
                    return true;
                }, 3000)

                if(timeout)
                    this.simulate();
            } else {
                return false
            }
        },

        randomMonsterPoints() {
            console.log(this.input);
            this.monsters.forEach(atributo => {
                let randm = Math.round(Math.random() * 3 + 1);
                atributo.pontos = randm;
            });
            //random money
            this.monsters.forEach(atributo => {
                let randm = Math.round(Math.random() * 99 + 1);
                atributo.money = randm;
            });            
        },

        resetGame() {
            if (this.player.life <= 0 && this.player.money === 0)
                this.player.life = 50;
            this.gameover = true;
            this.showButtons = false;
            this.winner = '';
            this.playerAction = '';
            this.monsterAction = '';            
        },        

        buyItem(item) {            
            if (this.player.money >= item.price) {
                this.player.money -= item.price;
                if (!this.player.items[`${item.nome}`]){                    
                    this.player.items[`${item.nome}`] = item;
                    this.player.items[`${item.nome}`]['qtd'] = 1;                    
                }else{
                    this.player.items[`${item.nome}`]['qtd']++;
                }                                    
            }
        },

        revanche() {

        },

        rndAction() {
            return Math.round(Math.random() * 2);            
        },

        background(monster, isLife = false) {            
            var bg = '';
            
            if(isLife)
                monster /= 10;

            monster = Math.floor(monster);
            
            switch (true) {                
                case monster <= 3:                
                    bg = 'var(--danger)';
                    break;                                
                case (monster >= 7 && monster <= 10):
                    bg = 'var(--good)'
                    break;
                default:
                    bg = 'var(--normal)'
                    break;
            }

            return bg;
        },

        run() {
            clearInterval(this.playerAgil);
            clearInterval(this.monsterAgil);

            if(this.player.skills.agilidade < this.fightingMonster.agilidade){
                this.player.life -= Math.round(Math.random() * this.fightingMonster.forca + this.fightingMonster.forca);
            }

            this.resetGame();

        },

        useItem(item) {
            switch (item) {
                case "Life":
                    if (this.player.items[item].qtd > 0) {                  
                        this.player.life += this.player.items[item].power;
                        if (this.player.life >= 100)
                            this.player.life = 100;
                        this.player.items[item].qtd--;
                    }
                    break;
                case "Bomb":
                    if (this.player.items[item].qtd > 0) {
                        this.fightingMonster.life -= Math.abs(this.player.items[item].power - this.fightingMonster.resistencia);
                        this.player.items[item].qtd--;
                    }
                    break;
                case "Sword":
                    if (this.player.items[item].qtd > 0) {
                        
                        if(this.player.skills.agilidade < this.fightingMonster.agilidade) {
                            let houveDano = Math.round(Math.random() * 1);
                            if(houveDano) {
                                this.fightingMonster.life -= Math.abs(this.player.items[item].power/2 - this.fightingMonster.resistencia);
                            }
                        } else {
                            let houveDano = Math.round(Math.random() * 1);
                            if (houveDano) {
                                this.fightingMonster.life -= Math.abs(this.player.items[item].power * 2 - this.fightingMonster.resistencia);
                            }
                        }


                        this.player.items[item].qtd--;
                    }
                    break;
            
                default:
                    break;
            }
        },

        simulate() {            
            let MA = this.rndAction();
            let PA = this.rndAction();

            this.playerAgil = setInterval(() => {
                PA = this.rndAction(); // 0 = stand, 1 = atack, 2 = defending
                this.playerAction = this.actions[PA];

                if(PA === 1) {                    
                    if(MA === 2)
                        this.fightingMonster.life -=Math.abs(this.player.skills.forca/2 - this.fightingMonster.resistencia);
                    else
                        this.fightingMonster.life -=Math.abs(this.player.skills.forca - this.fightingMonster.resistencia);                                   
                }

                this.checkGameover();
            }, 3500 / this.player.skills.agilidade);

            this.monsterAgil = setInterval(() => {
                MA = this.rndAction(); // 0 = stand, 1 = atack, 2 = defending
                this.monsterAction = this.actions[MA];

                if (MA === 1) {                    
                    if (PA === 2)
                        this.player.life -= Math.abs(this.fightingMonster.forca / 2 - this.player.skills.resistencia);
                    else
                        this.player.life -= Math.abs(this.fightingMonster.forca - this.player.skills.resistencia); 
                }

                this.checkGameover();

            }, 3500 / this.fightingMonster.agilidade);
        },

        checkGameover() {

            //checa se houve gameover
            if (this.player.life <= 0 || this.fightingMonster.life <= 0) {

                if (this.player.life <= 0) {
                    this.player.life = 0;
                    this.winner = this.fightingMonster.nome;
                    this.game.victory = false;
                    this.game.message = "Você morreu! <button onclick='window.location.reload()'>Clique aqui para recomeçar!</button>"                    
                } else if (this.fightingMonster.life <= 0) {
                    this.fightingMonster.life = 0;
                    this.winner = this.player.nome;
                    this.player.pontos = this.fightingMonster.pontos;
                    this.player.money += this.fightingMonster.money;
                    this.player.maxpontos += this.player.pontos;
                    
                    this.game.defeat++;
                    if (this.game.defeat >= Object.keys(this.monsters).length){
                        this.game.victory = true;
                        this.game.message = "Você é FODA! Matou todo mundo! <button onclick='location.reload()'>Clique aqui para recomeçar!</button>"
                    }                    
                }                

                // clearInterval(simulation);
                clearInterval(this.playerAgil);
                clearInterval(this.monsterAgil);
                this.showButtons = true;
            }
        }
    },
})